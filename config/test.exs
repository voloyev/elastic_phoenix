use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :elasticsearch_ui, ElasticsearchUiWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :elasticsearch_ui, ElasticsearchUi.Repo,
  username: "postgres",
  password: "",
  database: "elasticsearch_ui_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
