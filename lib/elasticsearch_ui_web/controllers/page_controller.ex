defmodule ElasticsearchUiWeb.PageController do
  use ElasticsearchUiWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
