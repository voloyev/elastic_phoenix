defmodule ElasticsearchUi.Repo do
  use Ecto.Repo,
    otp_app: :elasticsearch_ui,
    adapter: Ecto.Adapters.Postgres
end
